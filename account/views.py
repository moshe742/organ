from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.views.generic import DetailView, UpdateView, RedirectView


class AccountView(LoginRequiredMixin, DetailView):
    model = User
    slug_field = 'username'
    slug_url_kwarg = 'username'

    def get_context_data(self, **kwargs):
        context = super(AccountView, self).get_context_data(**kwargs)
        if 'user' in context:
            context['github'] = context['user'].social_auth.get(provider='github')
        return context


class AccountUpdate(LoginRequiredMixin, UpdateView):
    model = User
    slug_field = 'username'
    slug_url_kwarg = 'username'
    fields = ('first_name', 'last_name', 'email')
    success_url = '/account'


class ProfileView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        if self.request.user and self.request.user.is_authenticated:
            return '/account/{}/'.format(self.request.user.username)
        return '/'
